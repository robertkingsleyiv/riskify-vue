import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios'
import {signOut} from 'firebase/auth'
import {auth}from '@/helpers/firebaseInit'
import router from '@/router'

Vue.use(Vuex);
const baseAPI = process.env.VUE_APP_ROOT_API
export default new Vuex.Store({
  state: {
    authStatus: '',
    ui:{
      addressSearch:{
        showRecentSearches: false,
      }
    },

    addressToSearch: {
      street: '',
      zip: '',

    },
    realestateData: false,
    noListingTrigger: false,
    showLoading: false,
    userData:{
      recentlyViewedProperties: []
    }
  },
  getters: {
    getAuthStatus(state){
      return state.authStatus
    },
    getPropertyData(state){
      return state.realestateData
    }, 
    getAddressToSearch(state){
      return state.addressToSearch
    },
    getNoListingBoolean(state){
      return state.noListingTrigger
    },
    getLoadingBoolean(state){
      return state.showLoading
    },
    showRecentSearches(state){
      return state.ui.addressSearch.showRecentSearches
    }, recentSearches(state){
      return state.userData.recentlyViewedProperties
    }
  },
  mutations: {
    toggleShowRecentSearches(state){
      state.ui.addressSearch.showRecentSearches = !state.ui.addressSearch.showRecentSearches
    },
    setRealestateData(state, realestateData){
      state.realestateData = realestateData
    },
    setAddressToSearch(state, address){
      state.addressToSearch = address
    },
    setAuthStatus(state, status){
      state.authStatus = status
    },
    setNoListingTrigger(state, triggerBoolean){
      state.noListingTrigger = triggerBoolean
    },
    setShowLoading(state, loadingBoolean){
      state.showLoading = loadingBoolean
    },
    updateUserData(state, userData){
      state.userData = userData
    }  },
  actions: {
    getRealestateDataFromAddress ({state, commit, dispatch}) {
      commit("setNoListingTrigger", false)
      commit("setRealestateData", false)
      commit("setShowLoading", true)
      return new Promise((resolve, reject)=>{
        axios
        .post(`${baseAPI}/search/single/v2`, {
          addressData: {
            ...state.addressToSearch
          },
          requestedBy: auth.currentUser.uid
        })
        .then((resp) => {
          if (resp.data.error) {
           commit("setNoListingTrigger", true)
           commit('setRealestateData', false)
          commit("setShowLoading", false)
            dispatch('syncState')
          } else {
      
          console.log(resp.data)
          commit('setRealestateData', resp.data)
          commit("setShowLoading", false)
          commit("setNoListingTrigger", false)
          dispatch('syncState')
          resolve()
          }
        });
      })

    },
    getRealestateDataFromDatabase ({state, commit, dispatch}) {
      commit("setNoListingTrigger", false)
      commit("setRealestateData", false)
      commit("setShowLoading", true)
      return new Promise((resolve, reject)=>{
        axios
        .post(`${baseAPI}/search/single/database`, {
          addressData: {
            ...state.addressToSearch
          },
          requestedBy: auth.currentUser.uid
        })
        .then((resp) => {
          if (resp.data.error) {
           commit("setNoListingTrigger", true)
           commit('setRealestateData', false)
          commit("setShowLoading", false)
            dispatch('syncState')
          } else {
      
          console.log(resp.data)
          commit('setRealestateData', resp.data)
          commit("setShowLoading", false)
          commit("setNoListingTrigger", false)
          dispatch('syncState')
          resolve()
          }
        });
      })

    },
    syncState({state, commit}){
      return new Promise((resolve, reject)=>{
        axios
        .post(`${baseAPI}/user/sync`, {
          uid: auth.currentUser.uid
        })
        .then((resp) => {
          if (resp.data.error) {
            console.log(resp.data.error)
          } else {
      
          console.log(resp.data)
          commit('updateUserData', resp.data)
          resolve()
          }
        });
      })
    },
    signoutUser({state}){
      signOut(auth);
      router.push('/login')
      console.log(router)
    }
  },
  modules: {},
});
